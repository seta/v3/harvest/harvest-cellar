FROM python:3.10

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN useradd seta
ARG ROOT=/home/seta

WORKDIR $ROOT

COPY ./requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY ./config ./config
COPY ./repository ./repository
COPY ./src ./src
COPY ./*.py .

ENTRYPOINT ["python3"]