# Harvest Cellar


## Description
Download cellar publications per language.

## Environment

The following environment variables are required at runtime:
* LOG_LEVEL, defaults to INFO
* NLP_API_URL - root for NLP API (file parsing and embeddings)

For development, you can create a `.env` file with these variables.

## Download
Usage:
    `python3 download.py [options]`

Options:
    --help
    --lang Download language, "eng", "spa", "dan", "deu", "fra", "ita", "nld", "nor", "por", "swe", etc.
    --langISO2 Download language - alpha-2 code, "en", "es", "da", "de", "fr", "it", "nl", "no", "pt", "sv", etc.
    --resume Resume file download for language from the last metadata import, yes/no - no by default.
    --test Run in test mode, yes/no - no by default.

If `--lang` argument is set, than the alpha-2 code have to be set in the `--langISO2` argument.

Download metadata and parser documents for language:
```
python3 download.py --lang eng --langISO2 en
```

Download and parse document content for last metadata for language
```
python3 download.py --lang eng --langISO2 en --resume yes
```

Test mode:
```
python3 download.py --lang eng --langISO2 en --test yes
```

## Import

Import metadata of former downloaded documents.

Usage:
    `python3 import.py [options]`

Options:
    --help Show this message and exit.
    --import_dir Import directory, default=/media/backup/cellar/
    --lang Import language, one of "eng", "spa", "dan", "deu", "fra", "ita", "nld", "nor", "por", "swe", etc.
    --langISO2 Import language - alpha-2 code, one of "en", "es", "da", "de", "fr", "it", "nl", "no", "pt", "sv", etc.

If `--lang` argument is set, than the alpha-2 code have to be set in the `--langISO2` argument.

Import metadata documents for language:
```
python3 import.py --lang eng --langISO2 en
```

## Formatter

Format xml files to json:
```
python3 format_ingest.py
```

Test mode:
```
python3 format_ingest.py --test yes
```

### Build Image for Container Registry 
Build production image from the root of the project:
```
docker build -t code.europa.eu:4567/seta/v3/data/harvest/harvest-cellar:latest .
```

Push to remote container:
```
docker push code.europa.eu:4567/seta/v3/data/harvest/harvest-cellar:latest
```