import os
import configparser


class Config:
    """Application configuration"""

    EXTENSIONS_SKIP = []
    SQLITE_DIR = ""
    INGEST_SQLITE_DIR = ""
    DATA_DIR = ""
    OUTPUT_DIR = ""
    NLP_API_URL = ""

    def __init__(self, test_mode: bool = False):
        config_parser = configparser.ConfigParser(
            os.environ, interpolation=configparser.ExtendedInterpolation()
        )
        config_parser.read("./config/app.conf")

        sections = config_parser.sections()
        if len(sections) == 0:
            # pylint: disable-next=broad-exception-raised
            raise Exception("No configuration section found in the 'app.conf' file")

        section_name = "APP" if not test_mode else "APP_TEST"
        if section_name not in sections:
            # pylint: disable-next=broad-exception-raised
            raise Exception("APP must be one of " + str(sections))
        Config._init_app(config_section=config_parser[section_name])

        section_name = "CELLAR"
        if section_name not in sections:
            # pylint: disable-next=broad-exception-raised
            raise Exception("CELLAR must be one of " + str(sections))

        Config._init_cellar(config_section=config_parser[section_name])

    @staticmethod
    def _init_cellar(config_section: configparser.SectionProxy):
        """Initialize cellar config"""

        Config.EXTENSIONS_SKIP = config_section.get("EXTENSIONS_SKIP").split(",")
        Config.LIMIT = config_section.getint("LIMIT", fallback=100_000)

    @staticmethod
    def _init_app(config_section: configparser.SectionProxy):
        """Initialize  application config"""

        Config.SQLITE_DIR = config_section["SQLITE_DIR"]
        Config.INGEST_SQLITE_DIR = config_section["INGEST_SQLITE_DIR"]
        Config.DATA_DIR = config_section.get("DATA_DIR")
        Config.OUTPUT_DIR = config_section.get("OUTPUT_DIR")

        api_url = config_section.get("NLP_API_URL")
        Config.NLP_API_URL = os.getenv("NLP_API_URL", api_url)
