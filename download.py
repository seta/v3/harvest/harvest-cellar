"""
Download cellar publications.

Usage:
    python3 download.py [options]

Options:
    --help Show this message and exit.
    --lang Download language, "eng", "spa", "dan", "deu", "fra", "ita", "nld", "nor", "por", "swe", etc.
    --langISO2 Download language, "en", "es", "da", "de", "fr", "it", "nl", "no", "pt", "sv", etc.
    --resume Resume file download for language from the last metadata import, yes/no - no by default.
    --test Run in test mode, yes/no - no by default.
"""

import pathlib
import logging

import argparse

from dotenv import load_dotenv

from repository import dto, dto_sessions, dto_lang, utils
from config import config, log_init

from src import context as ctx
from src import cellar_downloader
from src import download_worker
from src.helpers import stopwatch as sw

load_dotenv()  # take environment variables from .env, if exists

log_init.init_logging()

parser = argparse.ArgumentParser(description="Download cellar publications.")

parser.add_argument(
    "--lang",
    help='Download language, optional one of "eng", "spa", "dan", "deu", "fra", "ita", "nld", "nor", "por", "swe", etc.',
)
parser.add_argument(
    "--langISO2",
    help='Download language - alpha-2 code, one of "en", "es", "da", "de", "fr", "it", "nl", "no", "pt", "sv", etc.',
)
parser.add_argument(
    "--resume",
    help="Resume download only of new documents for language, yes/no - default no.",
    default="no",
)
parser.add_argument("--test", help="Run in test mode, yes/no - default no.", default="no")

logger = logging.getLogger("download")


def init() -> tuple[str, str]:
    """Initialize the context."""

    args = parser.parse_args()

    lang = None if args.lang is None else args.lang.upper()
    lang_iso2 = None if args.langISO2 is None else args.langISO2.lower()
    test_mode = args.test is not None and args.test.lower() == "yes"
    resume_last = args.resume is not None and args.resume.lower() == "yes"

    if lang is None or lang_iso2 is None:
        raise ValueError("lang and langISO2 must be both set or both None.")

    configuration = config.Config(test_mode)

    pathlib.Path(configuration.SQLITE_DIR).mkdir(parents=True, exist_ok=True)
    pathlib.Path(configuration.DATA_DIR).mkdir(parents=True, exist_ok=True)

    session_uid = utils.generate_session_uid()

    main_db = utils.main_db(configuration.SQLITE_DIR, test_mode=test_mode)
    main_engine = dto.init_main_db(main_db, echo=False)

    publications_db = utils.publications_db(configuration.SQLITE_DIR, lang=lang, test_mode=test_mode)
    publications_engine = dto.init_publications_db(publications_db, echo=False)

    ctx.init_context(
        app_config=configuration,
        session_uid=session_uid,
        main_engine=main_engine,
        publications_engine=publications_engine,
        resume_last=resume_last,
        test=test_mode,
    )

    return lang, lang_iso2


def main():
    """Main function."""
    lang, lang_iso2 = init()

    dto_sessions.create_session(
        ids=True,
        lang=lang,
        lang_iso2=lang_iso2,
    )

    logger.info("Downloading publications for %s.", lang)

    if dto_lang.get_lang(lang) is None:
        dto_lang.insert_lang(lang, lang_iso2)

    stopwatch_lang = sw.Stopwatch()

    if not ctx.RESUME_LAST:
        inserts, updates, deleted = cellar_downloader.download_metadata(lang, lang_iso2)

        stopwatch_lang.stop()

        dto_sessions.create_stats(
            session_uid=ctx.SESSION_UID,
            stats="merge-insert-" + lang,
            counts=inserts,
            execution_seconds=stopwatch_lang.duration,
        )

        dto_sessions.create_stats(
            session_uid=ctx.SESSION_UID,
            stats="merge-update-" + lang,
            counts=updates,
            execution_seconds=stopwatch_lang.duration,
        )

        dto_sessions.create_stats(
            session_uid=ctx.SESSION_UID,
            stats="merge-delete-" + lang,
            counts=deleted,
            execution_seconds=stopwatch_lang.duration,
        )

        logger.info("Merged %d new documents, updates %d and deleted %d.", inserts, updates, deleted)

    logger.debug("Running download worker for %s...", lang)
    stopwatch_lang.restart()

    download_worker.delete_publications()

    downloads = download_worker.incremental_work(lang=lang)

    stopwatch_lang.stop()

    dto_sessions.create_stats(
        session_uid=ctx.SESSION_UID,
        stats="file-downloaded-" + lang,
        counts=downloads,
        execution_seconds=stopwatch_lang.duration,
    )
    dto_sessions.end_session(ctx.SESSION_UID)
    logger.info("Session ended.")


if __name__ == "__main__":
    try:
        main()
    except Exception as exc:
        logger.exception("error: %s", exc)

        dto_sessions.fail_session(ctx.SESSION_UID)

        raise SystemExit(1) from exc
    finally:
        if ctx.MAIN_ENGINE is not None:
            ctx.MAIN_ENGINE.dispose()
        if ctx.PUBLICATIONS_ENGINE is not None:
            ctx.PUBLICATIONS_ENGINE.dispose()
