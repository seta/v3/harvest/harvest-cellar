#! /usr/bin/env bash

python3 download.py --ids yes

python3 download.py --lang ENG --langISO2 en

python3 download.py --lang BUL --langISO2 bg
python3 download.py --lang CES --langISO2 cs
python3 download.py --lang DAN --langISO2 da
python3 download.py --lang DEU --langISO2 de

python3 download.py --lang ELL --langISO2 el
python3 download.py --lang SPA --langISO2 es
python3 download.py --lang EST --langISO2 et
python3 download.py --lang FIN --langISO2 fi

python3 download.py --lang FRA --langISO2 fr
python3 download.py --lang GLE --langISO2 ga
python3 download.py --lang HRV --langISO2 hr
python3 download.py --lang HUN --langISO2 hu

python3 download.py --lang ITA --langISO2 it
python3 download.py --lang LIT --langISO2 lt
python3 download.py --lang LAV --langISO2 lv
python3 download.py --lang MLT --langISO2 mt

python3 download.py --lang NLD --langISO2 nl
python3 download.py --lang POL --langISO2 pl
python3 download.py --lang POR --langISO2 pt
python3 download.py --lang RON --langISO2 ro

python3 download.py --lang SLK --langISO2 sk
python3 download.py --lang SLV --langISO2 sl
python3 download.py --lang SWE --langISO2 sv