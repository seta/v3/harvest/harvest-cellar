"""Format xml files to json for ingest."""

import pathlib
import argparse
import logging

from dotenv import load_dotenv

from config import config, log_init

from repository import dto, dto_lang, dto_sessions, utils

from src import seta_formatter
from src import context as ctx
from src.helpers import stopwatch as sw

from src import formatter_cache

load_dotenv()  # take environment variables from .env, if exists

log_init.init_logging()
logger = logging.getLogger("formatter")


parser = argparse.ArgumentParser(description="Format cellar publications.")
parser.add_argument(
    "--test", help="Run in test mode, yes/no - default no.", default="no"
)
parser.add_argument(
    "--chunk_complete_text",
    help="Format one chunk with complete text per document, yes/no - default no.",
    default="no",
)


def init() -> dict:
    """Initialize the context and returns the command arguments."""

    args = parser.parse_args()

    test_mode = args.test is not None and args.test.lower() in ["yes", "true"]
    chunk_complete_text = (
        args.chunk_complete_text is not None
        and args.chunk_complete_text.lower() in ["yes", "true"]
    )

    configuration = config.Config(test_mode=test_mode)

    pathlib.Path(configuration.SQLITE_DIR).mkdir(parents=True, exist_ok=True)
    pathlib.Path(configuration.INGEST_SQLITE_DIR).mkdir(parents=True, exist_ok=True)
    pathlib.Path(configuration.OUTPUT_DIR).mkdir(parents=True, exist_ok=True)

    main_db = utils.main_db(configuration.SQLITE_DIR, test_mode=test_mode)
    main_engine = dto.init_main_db(main_db, echo=False)

    ingest_db = utils.ingest_db(configuration.INGEST_SQLITE_DIR, test_mode=test_mode)
    ingest_engine = dto.init_ingest_db(ingest_db, echo=False)

    session_uid = utils.generate_session_uid()
    ctx.init_context(
        app_config=configuration,
        session_uid=session_uid,
        main_engine=main_engine,
        ingest_engine=ingest_engine,
        test=test_mode,
    )

    formatter_cache.init_cache()

    resource_types_length = formatter_cache.get_from_cache("resource_types_length")

    logger.info(
        "Cache initialized with %s resource types.",
        formatter_cache.get_from_cache("resource_types_length"),
    )

    if resource_types_length == 0:
        raise ValueError("Resource types cache is empty.")

    return {"test": test_mode, "chunk_complete_text": chunk_complete_text}


def main():
    """Main function."""

    args = init()

    langs = dto_lang.get_langs()

    for lang in langs:

        logger.info("Formatting language %s ...", lang.lang)

        dto_sessions.create_session(
            ids=False,
            lang=lang.lang,
            lang_iso2=lang.lang_iso2,
            formatter=True,
        )

        stopwatch = sw.Stopwatch()

        seta_formatter.build_documents(lang.lang)

        deleted = seta_formatter.ready_for_deletion(lang.lang)

        stopwatch.stop()

        logger.info(
            "Deleted %s documents for language %s in %s.",
            deleted,
            lang.lang,
            stopwatch.duration,
        )

        dto_sessions.create_stats(
            session_uid=ctx.SESSION_UID,
            stats="deleted-documents",
            counts=deleted,
            execution_seconds=stopwatch.duration,
        )

        stopwatch = sw.Stopwatch()

        total_processed, output_files = seta_formatter.start_formatter(
            lang.lang, lang.lang_iso2, complete_text=args["chunk_complete_text"]
        )

        stopwatch.stop()

        logger.info(
            "Formatting language %s, inserts: %s, file: %s in %s.",
            lang.lang,
            total_processed,
            output_files,
            stopwatch.duration,
        )

        dto_sessions.create_stats(
            session_uid=ctx.SESSION_UID,
            stats="json-entries",
            counts=total_processed,
            execution_seconds=stopwatch.duration,
        )

        dto_sessions.create_stats(
            session_uid=ctx.SESSION_UID,
            stats="output-files",
            counts=output_files,
            execution_seconds=stopwatch.duration,
        )


if __name__ == "__main__":
    try:
        main()
    except Exception as exc:
        logger.exception("error: %s", exc)

        raise SystemExit(1) from exc
    finally:
        formatter_cache.close_cache()
        if ctx.MAIN_ENGINE is not None:
            ctx.MAIN_ENGINE.dispose()
        if ctx.INGEST_ENGINE is not None:
            ctx.INGEST_ENGINE.dispose()
