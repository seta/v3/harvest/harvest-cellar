"""Import metadata from csv files to SQLite databases."""

import pathlib
import argparse
import logging

from dotenv import load_dotenv

from config import config, log_init

from repository import dto, dto_sessions, dto_lang, utils
from src import cellar_import
from src import context as ctx
from src.helpers import stopwatch as sw

load_dotenv()  # take environment variables from .env, if exists

log_init.init_logging()
logger = logging.getLogger("import")

parser = argparse.ArgumentParser(description="Import existing cellar publications.")

parser.add_argument(
    "--import_dir", help="Import directory.", default="/media/backup/cellar/"
)
parser.add_argument(
    "--lang",
    help='Import language, one of "eng", "spa", "dan", "deu", "fra", "ita", "nld", "nor", "por", "swe", etc.',
)
parser.add_argument(
    "--langISO2",
    help='Import language - alpha-2 code, one of "en", "es", "da", "de", "fr", "it", "nl", "no", "pt", "sv", etc.',
)
parser.add_argument(
    "--test", help="Run in test mode, yes/no - default no.", default="no"
)


def init() -> tuple[str, str, str]:
    """Initialize the context."""
    args = parser.parse_args()

    import_dir = args.import_dir

    lang = None if args.lang is None else args.lang.upper()
    lang_iso2 = None if args.langISO2 is None else args.langISO2.lower()
    test_mode = args.test is not None and args.test.lower() == "yes"

    if lang is None or lang_iso2 is None:
        raise ValueError("lang and langISO2 must be both set or both None.")

    configuration = config.Config(test_mode)

    pathlib.Path(configuration.SQLITE_DIR).mkdir(parents=True, exist_ok=True)
    pathlib.Path(configuration.DATA_DIR).mkdir(parents=True, exist_ok=True)

    main_db = utils.main_db(configuration.SQLITE_DIR, test_mode=test_mode)
    main_engine = dto.init_main_db(main_db, echo=False)

    publications_db = utils.publications_db(
        configuration.SQLITE_DIR, lang=lang, test_mode=test_mode
    )

    session_uid = utils.generate_session_uid()
    ctx.init_context(
        app_config=configuration,
        session_uid=session_uid,
        main_engine=main_engine,
        publications_engine=dto.init_publications_db(publications_db),
        test=test_mode,
    )

    return import_dir, lang, lang_iso2


def main():
    """Main function."""

    import_dir, lang, lang_iso2 = init()

    dto_sessions.create_session(
        ids=False,
        lang=lang,
        lang_iso2=lang_iso2,
    )

    logger.info("Importing publications for %s.", lang)

    if not dto_lang.get_lang(lang):
        dto_lang.insert_lang(lang, lang_iso2)

    stopwatch_lang = sw.Stopwatch()

    inserted = cellar_import.import_publications(lang, import_dir)

    stopwatch_lang.stop()

    dto_sessions.create_stats(
        session_uid=ctx.SESSION_UID,
        stats="merge-insert-" + lang,
        counts=inserted,
        execution_seconds=stopwatch_lang.duration,
    )

    dto_sessions.end_session(ctx.SESSION_UID)
    logger.info("Session ended.")


if __name__ == "__main__":
    try:
        main()
    except Exception as exc:
        logger.exception("error: %s", exc)

        dto_sessions.fail_session(ctx.SESSION_UID)

        raise SystemExit(1) from exc
    finally:
        if ctx.MAIN_ENGINE is not None:
            ctx.MAIN_ENGINE.dispose()
        if ctx.PUBLICATIONS_ENGINE is not None:
            ctx.PUBLICATIONS_ENGINE.dispose()
