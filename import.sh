#! /usr/bin/env bash

python3 import.py --ids yes

python3 import.py --lang ENG --langISO2 en

python3 import.py --lang BUL --langISO2 bg
python3 import.py --lang CES --langISO2 cs
python3 import.py --lang DAN --langISO2 da
python3 import.py --lang DEU --langISO2 de

python3 import.py --lang ELL --langISO2 el
python3 import.py --lang SPA --langISO2 es
python3 import.py --lang EST --langISO2 et
python3 import.py --lang FIN --langISO2 fi

python3 import.py --lang FRA --langISO2 fr
python3 import.py --lang GLE --langISO2 ga
python3 import.py --lang HRV --langISO2 hr
python3 import.py --lang HUN --langISO2 hu

python3 import.py --lang ITA --langISO2 it
python3 import.py --lang LIT --langISO2 lt
python3 import.py --lang LAV --langISO2 lv
python3 import.py --lang MLT --langISO2 mt

python3 import.py --lang NLD --langISO2 nl
python3 import.py --lang POL --langISO2 pl
python3 import.py --lang POR --langISO2 pt
python3 import.py --lang RON --langISO2 ro

python3 import.py --lang SLK --langISO2 sk
python3 import.py --lang SLV --langISO2 sl
python3 import.py --lang SWE --langISO2 sv