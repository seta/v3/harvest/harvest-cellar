import sqlalchemy as db
from .models import main_models, ingest_models, publication_models


def init_main_db(sqlite_db: str, echo: bool = False) -> db.engine.Engine:
    """Gets the engine."""

    engine = db.create_engine(sqlite_db, echo=echo)
    main_models.Base.metadata.create_all(engine)

    return engine


def init_publications_db(sqlite_db: str, echo: bool = False) -> db.engine.Engine:
    """Gets the engine."""

    engine = db.create_engine(sqlite_db, echo=echo)

    publication_models.Base.metadata.create_all(engine)

    return engine


def init_ingest_db(sqlite_db: str, echo: bool = False) -> db.engine.Engine:
    """Gets the engine."""

    engine = db.create_engine(sqlite_db, echo=echo)

    ingest_models.Base.metadata.create_all(engine)

    return engine
