"""Data Transfer Object (DTO) for the database."""

import sqlalchemy as db
from sqlalchemy.orm import Session
from sqlalchemy.sql import text

from src import context as ctx
from .models import file_status as fs, publication_models as models


def build_documents(engine: db.engine.Engine = None):
    """Build documents."""

    if engine is None:
        engine = ctx.PUBLICATIONS_ENGINE

    with engine.connect() as connection:
        # mark as deleted documents that have no valid publication
        connection.execute(
            text(
                f"""
                UPDATE documents
                SET status = '{fs.DocumentStatus.DELETED}',
                    ingest_status = '{fs.IngestStatus.DELETE}'
                WHERE doc_id NOT IN (
                    SELECT doc_id
                    FROM publications
                    WHERE status IN ('{fs.PublicationStatus.DOWNLOADED}', '{fs.PublicationStatus.IMPORTED}', '{fs.PublicationStatus.FORMATTED}')
                    GROUP BY doc_id
                );
            """
            )
        )
        connection.commit()

        # update documents status for changed or re-download publication
        connection.execute(
            text(
                f"""
                UPDATE documents as d
                SET publication_uid = upd.uid,
                    status = (CASE WHEN upd.status = '{fs.PublicationStatus.IMPORTED}' THEN '{fs.DocumentStatus.READY_FOR_INGEST}' ELSE '{fs.DocumentStatus.FORMAT}' END),
                    ingest_status = '{fs.IngestStatus.UPDATE}'
                FROM (
                    SELECT r.doc_id, r.uid, r.status                    
                    FROM (
                            SELECT p.uid, p.doc_id, p.status,
                                RANK() OVER (partition by p.doc_id order by p.format_priority DESC, p.uid) AS rnk
                            FROM publications p
                                INNER JOIN documents d ON p.doc_id = d.doc_id
                            WHERE p.status IN ('{fs.PublicationStatus.DOWNLOADED}', '{fs.PublicationStatus.IMPORTED}', '{fs.PublicationStatus.FORMATTED}')
                        ) r
                    WHERE r.rnk = 1 
                    ) as upd 
                WHERE d.doc_id = upd.doc_id AND (d.publication_uid != upd.uid OR upd.status = '{fs.PublicationStatus.DOWNLOADED}');
            """
            )
        )
        connection.commit()

        # insert new documents from downloaded or imported publications
        connection.execute(
            text(
                f"""
                INSERT INTO documents (doc_id, publication_uid, status, ingest_status)
                SELECT r.doc_id, r.uid, 
                    CASE WHEN r.status = '{fs.PublicationStatus.DOWNLOADED}' 
                        THEN '{fs.DocumentStatus.FORMAT}' 
                        ELSE '{fs.DocumentStatus.READY_FOR_INGEST}' END, 
                    CASE WHEN r.status = '{fs.PublicationStatus.DOWNLOADED}' THEN '{fs.IngestStatus.NEW}' ELSE '{fs.IngestStatus.IMPORTED}' END
                FROM (
                    SELECT uid, doc_id, status,
                        RANK() OVER (partition by doc_id order by format_priority DESC, uid) AS rnk
                        FROM publications
                        WHERE status IN ('{fs.PublicationStatus.DOWNLOADED}', '{fs.PublicationStatus.IMPORTED}')
                    ) r
                    LEFT JOIN documents d ON r.doc_id = d.doc_id
                WHERE r.rnk = 1 AND d.doc_id IS NULL;
            """
            )
        )
        connection.commit()


def get_deleted_documents(
    engine: db.engine.Engine,
) -> list[models.Document]:
    """Get deleted documents."""

    with Session(engine) as session:

        return session.query(models.Document).filter(models.Document.status == fs.DocumentStatus.DELETED).all()


def batch_set_ready_for_deletion(doc_ids: list[str], engine: db.engine.Engine):
    """Sets documents as ready for deletion."""

    with Session(engine) as session:
        session.query(models.Document).filter(
            models.Document.doc_id.in_(doc_ids),
            models.Document.status == fs.DocumentStatus.DELETED,
        ).update({models.Document.status: fs.DocumentStatus.READY_FOR_DELETION})
        session.commit()


def get_format_count(engine: db.engine.Engine) -> int:
    """Get the count of documents to format."""

    with Session(engine) as session:
        return session.query(models.Document).filter(models.Document.status == fs.DocumentStatus.FORMAT).count()


def get_format_documents(engine: db.engine.Engine, limit: int) -> list[models.Document]:
    """Get documents to format."""

    with Session(engine) as session:
        return (
            session.query(models.Document)
            .filter(models.Document.status == fs.DocumentStatus.FORMAT)
            .order_by(models.Document.doc_id)
            .limit(limit)
            .all()
        )


def batch_processed(
    engine: db.engine.Engine,
    update_doc_ids: list[str],
    error_doc_ids: list[str],
    publication_uids: list[str],
    output_file_name: str,
):
    """Sets a document as ready for ingestion."""

    with Session(engine) as session:

        if update_doc_ids:
            session.query(models.Document).filter(
                models.Document.doc_id.in_(update_doc_ids),
                models.Document.status == fs.DocumentStatus.FORMAT,
            ).update(
                {
                    models.Document.status: fs.DocumentStatus.READY_FOR_INGEST,
                    models.Document.output_file_name: output_file_name,
                }
            )

            session.commit()

        if error_doc_ids:
            session.query(models.Document).filter(
                models.Document.doc_id.in_(error_doc_ids),
                models.Document.status == fs.DocumentStatus.FORMAT,
            ).update({models.Document.status: fs.DocumentStatus.ERROR})

            session.commit()

        if publication_uids:
            session.query(models.Publication).filter(models.Publication.uid.in_(publication_uids)).update(
                {models.Publication.status: fs.PublicationStatus.FORMATTED}
            )

            session.commit()
