"""Data Transfer Object for Ingestion."""

import datetime as dt
from sqlalchemy.orm import Session

from src import context as ctx
from .models import ingest_models as models


def insert(file_path: str, entries: int, lang: str):
    """Insert output file."""

    ingest_file = models.IngestionFile(
        file_path=file_path,
        entries=entries,
        status="pending",
        language=lang,
        created_at=dt.datetime.now(dt.timezone.utc),
    )

    with Session(ctx.INGEST_ENGINE) as session:

        session.add(ingest_file)
        session.commit()

def batch_insert_deleted(doc_ids: list[str]):
    """Batch insert doc_ids for deletion."""

    deleted_docs = [
        models.DeletionDocument(doc_id=doc_id, status="pending", created_at=dt.datetime.now(dt.timezone.utc))
        for doc_id in doc_ids
    ]

    with Session(ctx.INGEST_ENGINE) as session:

        session.add_all(deleted_docs)
        session.commit()