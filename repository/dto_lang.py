from sqlalchemy.orm import Session

from src import context as ctx
from .models import main_models as models


def insert_lang(lang: str, lang_iso2: str) -> models.LangCatalogue:
    """Insert a language entry in the database."""

    with Session(ctx.MAIN_ENGINE) as session:
        lang = models.LangCatalogue(
            lang=lang,
            lang_iso2=lang_iso2,
        )

        session.add(lang)
        session.commit()

    return lang


def get_lang(lang: str) -> models.LangCatalogue:
    """Get a language entry from the database."""

    with Session(ctx.MAIN_ENGINE) as session:
        lang = (
            session.query(models.LangCatalogue)
            .filter(models.LangCatalogue.lang == lang)
            .first()
        )

    return lang


def get_langs() -> list[models.LangCatalogue]:
    """Get all language entries from the database."""

    with Session(ctx.MAIN_ENGINE) as session:
        langs = (
            session.query(models.LangCatalogue)
            .order_by(models.LangCatalogue.lang)
            .all()
        )

    return langs
