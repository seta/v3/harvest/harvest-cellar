"""Data Transfer Object (DTO) for the database."""

import sqlalchemy as db
from sqlalchemy.orm import Session

from src import context as ctx
from .models import publication_models as models
from .models import file_status as fs


###### Publications CRUD ######


def bulk_insert(publications: list[dict]):
    """Bulk insert."""

    with Session(ctx.PUBLICATIONS_ENGINE) as session:

        session.execute(db.insert(models.Publication), publications)
        session.commit()


def bulk_update_status(ids: list[str], status: str):
    """Bulk update publications status."""

    with Session(ctx.PUBLICATIONS_ENGINE) as session:

        session.query(models.Publication).filter(models.Publication.uid.in_(ids)).update(
            {models.Publication.status: status},
            synchronize_session=False,
        )
        session.commit()


def update_status(uid: str, status: str):
    """Updates a document status."""

    with Session(ctx.PUBLICATIONS_ENGINE) as session:

        session.query(models.Publication).filter(models.Publication.uid == uid).update(
            {models.Publication.status: status}
        )
        session.commit()


def set_downloaded(
    uid: str,
    file_path: str,
    content_size: int | None = None,
    file_size: int | None = None,
):
    """Sets a document as downloaded."""

    update_fields = {
        models.Publication.status: fs.PublicationStatus.DOWNLOADED,
        models.Publication.file_path: file_path,
    }

    if content_size is not None:
        update_fields[models.Publication.content_size] = content_size

    if file_size is not None:
        update_fields[models.Publication.file_size] = file_size

    with Session(ctx.PUBLICATIONS_ENGINE) as session:

        session.query(models.Publication).filter(models.Publication.uid == uid).update(update_fields)
        session.commit()


def set_missing(ids: list[str], session_uid: str):
    """Update a documents status as missing."""

    with Session(ctx.PUBLICATIONS_ENGINE) as session:

        session.query(models.Publication).filter(models.Publication.uid.in_(ids)).update(
            {
                models.Publication.status: fs.PublicationStatus.MISSING,
                models.Publication.updated_session_uid: session_uid,
            },
            synchronize_session=False,
        )
        session.commit()


def update(document: dict, session_uid: str):
    """Update a publication."""

    with Session(ctx.PUBLICATIONS_ENGINE) as session:

        session.query(models.Publication).filter(models.Publication.uid == document["uid"]).update(
            {
                models.Publication.uri: document["uri"],
                models.Publication.project_uid: document["project_uid"],
                models.Publication.date: document["date"],
                models.Publication.year: document["year"],
                models.Publication.type: document["type"],
                models.Publication.ids: document["ids"],
                models.Publication.subjects: document["subjects"],
                models.Publication.do_not_index: document["do_not_index"],
                models.Publication.resource_legal_id_sector: document["resource_legal_id_sector"],
                models.Publication.force: document["force"],
                models.Publication.title: document["title"],
                models.Publication.mime: document["mime"],
                models.Publication.format: document["format"],
                models.Publication.file_extension: document["file_extension"],
                models.Publication.format_priority: document["format_priority"],
                models.Publication.hash: document["hash"],
                models.Publication.status: fs.PublicationStatus.UPDATED,
                models.Publication.updated_session_uid: session_uid,
            }
        )
        session.commit()


#############


def get_all_ids() -> list[dict]:
    """Get all unique ids."""

    with Session(ctx.PUBLICATIONS_ENGINE) as session:

        results = (
            session.query(models.Publication.uid, models.Publication.status, models.Publication.hash)
            .order_by(models.Publication.uid)
            .all()
        )
        if results:
            return [{"uid": result[0], "status": result[1], "hash": result[2]} for result in results]

        return []


def any_publications() -> bool:
    """Check if there are any documents."""

    with Session(ctx.PUBLICATIONS_ENGINE) as session:

        return session.query(models.Publication).count() > 0


def count_for_download() -> int:
    """Count publications for download."""

    with Session(ctx.PUBLICATIONS_ENGINE) as session:

        return (
            session.query(models.Publication)
            .filter(models.Publication.status.in_([fs.PublicationStatus.NEW, fs.PublicationStatus.UPDATED]))
            .count()
        )


def get_all_for_download(limit: int = 100) -> list[models.Publication]:
    """Get all new documents."""

    with Session(ctx.PUBLICATIONS_ENGINE) as session:

        return (
            session.query(models.Publication)
            .filter(models.Publication.status.in_([fs.PublicationStatus.NEW, fs.PublicationStatus.UPDATED]))
            .order_by(models.Publication.uid)
            .limit(limit)
            .all()
        )


def get_missing() -> list[models.Publication]:
    """Get missing documents."""

    with Session(ctx.PUBLICATIONS_ENGINE) as session:

        return (
            session.query(models.Publication)
            .filter(models.Publication.status == fs.PublicationStatus.MISSING)
            .order_by(models.Publication.uid)
            .all()
        )
