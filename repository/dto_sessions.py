"""Data Transfer Object (DTO) for the database."""

import datetime as dt
from sqlalchemy.orm import Session

from src import context as ctx
from .models import main_models as models


def create_session(
    ids: bool, lang: str, lang_iso2: str, formatter: bool = False
) -> models.DownloadSession:
    """Create a session entry in the database."""

    with Session(ctx.MAIN_ENGINE, expire_on_commit=False) as session:
        download_session = models.DownloadSession(
            uid=ctx.SESSION_UID,
            status="running",
            download_ids=ids,
            download_lang=lang,
            download_lang_iso2=lang_iso2,
            formatter=formatter,
            start=dt.datetime.now(dt.timezone.utc),
        )

        session.add(download_session)
        session.commit()

    return download_session


def end_session(session_uid: str):
    """End a session entry in the database."""

    with Session(ctx.MAIN_ENGINE) as session:
        session.query(models.DownloadSession).filter(
            models.DownloadSession.uid == session_uid
        ).update({"status": "completed", "end": dt.datetime.utcnow()})
        session.commit()


def fail_session(session_uid: str):
    """End a session entry in the database."""

    with Session(ctx.MAIN_ENGINE) as session:
        session.query(models.DownloadSession).filter(
            models.DownloadSession.uid == session_uid
        ).update({"status": "failed", "end": dt.datetime.utcnow()})
        session.commit()


def create_stats(
    session_uid: str,
    stats: str,
    counts: int,
    execution_seconds: float,
):
    """Create a stats entry in the database."""

    with Session(ctx.MAIN_ENGINE) as session:
        stats = models.SessionStats(
            download_session_uid=session_uid,
            stats=stats,
            counts=counts,
            execution_seconds=execution_seconds,
        )

        session.add(stats)
        session.commit()

    return stats
