from enum import Enum


class PublicationStatus(str, Enum):
    NEW = "new"
    UPDATED = "updated"

    IMPORTED = "imported"
    DOWNLOADED = "downloaded"
    FORMATTED = "formatted"

    MISSING = "missing"
    DELETED = "deleted"

    INVALID = "invalid"
    ERROR_ON_DOWNLOAD = "error_on_download"
    ERROR_ON_PARSE = "error_on_parse"

    def __str__(self):
        return str(self.value)


class DocumentStatus(str, Enum):

    FORMAT = "format"
    ERROR = "error"
    DELETED = "deleted"

    READY_FOR_INGEST = "ready_for_ingest"
    READY_FOR_DELETION = "ready_for_deletion"
    IGNORE_FOR_INGEST = "ignore_for_ingest"


class IngestStatus(str, Enum):

    NEW = "new"
    UPDATE = "update"
    DELETE = "delete"
    IMPORTED = "imported"
