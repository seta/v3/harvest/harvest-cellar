"""Models for repository."""

import datetime as dt
from sqlalchemy import DATETIME, INTEGER, String
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column


class Base(DeclarativeBase):
    """Base class for all models in the repository."""

    pass


class IngestionFile(Base):
    """Model for the ingestion file table."""

    __tablename__ = "ingestion_files"

    id: Mapped[int] = mapped_column(INTEGER, primary_key=True, autoincrement=True)
    file_path: Mapped[str] = mapped_column(String, nullable=False)
    entries: Mapped[int] = mapped_column(INTEGER, nullable=False, default=0)
    status: Mapped[str] = mapped_column(String, nullable=False)
    language: Mapped[str] = mapped_column(String, nullable=True)
    created_at: Mapped[dt.datetime] = mapped_column(DATETIME, nullable=False)
    updated_at: Mapped[dt.datetime] = mapped_column(DATETIME, nullable=True)

    def __repr__(self) -> str:
        return f"<IngestionFile(id={self.id}, file_path={self.file_path}, status={self.status})>"


class DeletionDocument(Base):
    """Model for the deletion document table."""

    __tablename__ = "deletion_documents"

    id: Mapped[int] = mapped_column(INTEGER, primary_key=True, autoincrement=True)
    doc_id: Mapped[str] = mapped_column(String, nullable=False)
    status: Mapped[str] = mapped_column(String, nullable=False)
    created_at: Mapped[dt.datetime] = mapped_column(DATETIME, nullable=False)
    updated_at: Mapped[dt.datetime] = mapped_column(DATETIME, nullable=True)

    def __repr__(self) -> str:
        return f"<DeletionDocument(id={self.id}, doc_id={self.doc_id}, status={self.status})>"
