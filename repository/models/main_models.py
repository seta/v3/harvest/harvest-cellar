"""Models for repository."""

import datetime as dt
from sqlalchemy import DATETIME, String, INTEGER, FLOAT, BOOLEAN
from sqlalchemy import ForeignKey
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column


class Base(DeclarativeBase):
    """Base class for all models in the repository."""

    pass


class DownloadSession(Base):
    """Model for the session table."""

    __tablename__ = "sessions"

    uid: Mapped[str] = mapped_column(String, primary_key=True)
    status: Mapped[str]
    download_ids: Mapped[bool]
    download_lang: Mapped[str] = mapped_column(String(3), nullable=True)
    download_lang_iso2: Mapped[str] = mapped_column(String(2), nullable=True)
    formatter: Mapped[bool] = mapped_column(BOOLEAN, default=False)
    start: Mapped[dt.datetime] = mapped_column(DATETIME)
    end: Mapped[dt.datetime] = mapped_column(DATETIME, nullable=True)

    def __repr__(self) -> str:
        return f"<DownloadSession(uid={self.uid}, status={self.status}, download_ids={self.download_ids}, download_lang={self.download_lang})>"


class SessionStats(Base):
    """Model for the session stats table."""

    __tablename__ = "stats"

    id: Mapped[int] = mapped_column(INTEGER, primary_key=True, autoincrement=True)
    download_session_uid: Mapped[str] = mapped_column(
        String, ForeignKey("sessions.uid"), nullable=False
    )
    stats: Mapped[str]
    counts: Mapped[int] = mapped_column(INTEGER, nullable=True)
    execution_seconds: Mapped[float] = mapped_column(FLOAT, nullable=True)

    def __repr__(self) -> str:
        return f"<SessionStats(id={self.id}, download_session_uid={self.download_session_uid}, stats={self.stats}>"


class LangCatalogue(Base):
    """Model for the language catalogue table."""

    __tablename__ = "lang_catalogue"

    lang: Mapped[str] = mapped_column(
        String(3),
        primary_key=True,
    )
    lang_iso2: Mapped[str] = mapped_column(String(2), nullable=False)

    def __repr__(self) -> str:
        return f"<LangCatalogue(lang={self.lang}, lang_iso2={self.lang_iso2})>"
