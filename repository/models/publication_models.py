from sqlalchemy import INTEGER, String, ForeignKey
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column
from sqlalchemy.orm import relationship


class Base(DeclarativeBase):
    """Base class for all models in the repository."""

    pass


class Publication(Base):

    __tablename__ = "publications"

    uid: Mapped[str] = mapped_column(String, primary_key=True, nullable=False)

    project_uid: Mapped[str] = mapped_column(String, nullable=False)
    uri: Mapped[str] = mapped_column(String, nullable=False)
    do_not_index: Mapped[str] = mapped_column(String, nullable=True)
    resource_legal_id_sector: Mapped[str] = mapped_column(String, nullable=True)
    force: Mapped[str] = mapped_column(String, nullable=True)
    date: Mapped[str] = mapped_column(String, nullable=True)
    type: Mapped[str] = mapped_column(String, nullable=True)
    ids: Mapped[str] = mapped_column(String, nullable=True)
    subjects: Mapped[str] = mapped_column(String, nullable=True)
    year: Mapped[str] = mapped_column(String, nullable=False)

    link: Mapped[str] = mapped_column(String, unique=True, nullable=False)
    lang: Mapped[str] = mapped_column(String, nullable=False)
    format: Mapped[str] = mapped_column(String, nullable=True)
    mime: Mapped[str] = mapped_column(String, nullable=True)
    title: Mapped[str] = mapped_column(String, nullable=True)

    file_path: Mapped[str] = mapped_column(String, nullable=True)
    content_size: Mapped[int] = mapped_column(INTEGER, nullable=True)
    file_size: Mapped[int] = mapped_column(INTEGER, nullable=True)
    file_extension: Mapped[str] = mapped_column(String, nullable=True)

    doc_id: Mapped[str] = mapped_column(String, nullable=False)
    format_priority: Mapped[int] = mapped_column(INTEGER, nullable=False, default=0)
    created_session_uid: Mapped[str] = mapped_column(String, nullable=False)
    updated_session_uid: Mapped[str] = mapped_column(String, nullable=True)

    status: Mapped[str] = mapped_column(String, nullable=False)
    hash: Mapped[str] = mapped_column(String, nullable=False)

    def __repr__(self) -> str:
        return f"<Publication(uid={self.uid}>"


class Document(Base):

    __tablename__ = "documents"

    doc_id: Mapped[str] = mapped_column(String, primary_key=True, nullable=False)

    publication_uid: Mapped[str] = mapped_column(ForeignKey("publications.uid"), nullable=True, unique=True)
    output_file_name: Mapped[str] = mapped_column(String, nullable=True)

    publication: Mapped[Publication | None] = relationship(
        "Publication", foreign_keys=[publication_uid], viewonly=True, lazy="joined"
    )

    status: Mapped[str] = mapped_column(String, nullable=False)
    ingest_status: Mapped[str] = mapped_column(String, nullable=False)

    def __repr__(self) -> str:
        return f"<Document(doc_id={self.doc_id}>"
