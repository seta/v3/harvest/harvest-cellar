import datetime as dt


def generate_session_uid() -> str:
    """Generate a session uid."""

    now = dt.datetime.utcnow()
    return now.strftime("%Y%m%d%H%M%S")


def main_db(dir_path: str, test_mode: bool = False) -> str:
    """Gets sqlite database connection string."""

    if test_mode:
        return f"sqlite:///{dir_path}/test_cellar.db"

    return f"sqlite:///{dir_path}/cellar.db"


def lang_db(dir_path: str, lang: str, test_mode: bool = False) -> str:
    """Gets sqlite database connection string."""

    if test_mode:
        return f"sqlite:///{dir_path}/test_{lang}.db"

    return f"sqlite:///{dir_path}/last_session_{lang}.db"


def ingest_db(dir_path: str, test_mode: bool = False) -> str:
    """Gets sqlite database connection string."""

    if test_mode:
        return f"sqlite:///{dir_path}/test_ingestion.db"

    return f"sqlite:///{dir_path}/ingestion.db"


def publications_db(dir_path: str, lang: str, test_mode: bool = False) -> str:
    """Gets sqlite database connection string."""

    if test_mode:
        return f"sqlite:///{dir_path}/test_publications_{lang}.db"

    return f"sqlite:///{dir_path}/publications_{lang}.db"
