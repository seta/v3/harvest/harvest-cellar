import logging
import pathlib
import csv
import time
from SPARQLWrapper import SPARQLWrapper, CSV

from src import context as ctx, constants as const
from src.helpers import stopwatch as sw, cellar_query, cellar_prefixes
from src.helpers import utils

from repository import dto_publications
from repository.models import file_status as fs

logger = logging.getLogger("download")

sparql = SPARQLWrapper(const.SPARQL_ENDPOINT, returnFormat=CSV)
sparql.setTimeout(1200)


def download_metadata(lang: str, lang_iso2: str) -> tuple[int, int, int]:
    """Download lang metadata."""

    csv_new_output, csv_update_output, csv_remove_output = _get_paths(lang_iso2)

    cnt = 0
    inserts = 0
    updates = 0

    tries = 1

    metadata_ids = []
    insert_documents = []

    def _bulk_insert_documents():
        # Bulk insert

        nonlocal insert_documents, inserts, csv_new_output

        if insert_documents:
            logger.debug(
                "Inserting %d documents into the collection.", len(insert_documents)
            )
            inserts += len(insert_documents)
            try:
                dto_publications.bulk_insert(insert_documents)
                _write_to_csv(csv_new_output, insert_documents)
            except Exception:
                pathlib.Path(csv_new_output).unlink(missing_ok=True)
                raise
            finally:
                insert_documents = []

    prefixes = cellar_prefixes.get_prefixes()
    logger.info("prefixes no: %s", len(prefixes))

    for prefix in prefixes:

        logger.info("Running query for prefix: %s ...", prefix)

        sparql_query = cellar_query.get_query(lang, lang_iso2, prefix)
        sparql.setQuery(sparql_query)

        # logger.debug("sparql_query: %s", sparql_query)

        stopwatch = sw.Stopwatch()
        try:
            if tries > 1:
                logger.warning("Retry %s for prefix: %s", tries, prefix)
                time.sleep(5)

            result = sparql.queryAndConvert()
            tries = 1  # reset tries
        except Exception as e:
            logger.exception("error: %s", e)
            if tries <= 4:
                tries += 1
                continue

            raise

        lines = result.decode("utf-8").splitlines()
        size = len(lines) - 1  # header row

        logger.info("prefix: %s, size: %s", prefix, size)

        if size <= 0:
            continue

        cnt += size
        doc_uids_cache = {}

        csv_data = csv.reader(lines)

        fields = next(csv_data)  # header row
        logger.debug("fields: %s", fields)

        insert_documents = []
        collection = dto_publications.get_all_ids()

        for row in csv_data:
            row_data = dict(zip(fields, row))

            doc_uid = row_data["link_exp"][46:]
            metadata_ids.append(doc_uid)

            if doc_uids_cache.get(doc_uid, None) is not None:
                continue

            doc_uids_cache[doc_uid] = 1

            document = utils.build_meta_row(row_data, include_project_info=True)
            document["lang"] = lang
            document["status"] = fs.PublicationStatus.NEW
            document["created_session_uid"] = ctx.SESSION_UID
            document["hash"] = utils.compute_publication_hash(document)

            existing_doc = utils.binary_search_dicts(collection, "uid", doc_uid)
            if existing_doc is not None:
                if existing_doc["hash"] != document["hash"] or existing_doc[
                    "status"
                ] in [fs.PublicationStatus.DELETED, fs.PublicationStatus.MISSING]:
                    updates += 1
                    dto_publications.update(document, ctx.SESSION_UID)
                    _write_to_csv(csv_update_output, [document])
                continue

            insert_documents.append(document)

            if len(insert_documents) >= 50_000:
                _bulk_insert_documents()

        _bulk_insert_documents()

        stopwatch.stop()
        logger.info(
            "prefix: %s, inserts: %s, execution_time: %s",
            prefix,
            inserts,
            str(stopwatch),
        )

        if ctx.TEST:
            break

    deleted = 0
    if ctx.TEST is False:
        deleted = _delete_documents(csv_remove_output, metadata_ids)

    return inserts, updates, deleted


def _delete_documents(csv_remove_output: str, metadata_ids: list[str]) -> int:
    """Deletes the documents that are not in the metadata"""
    deleted = 0

    delete_documents = []
    metadata_ids.sort()

    collection = dto_publications.get_all_ids()

    for doc in collection:
        if doc["status"] not in [fs.PublicationStatus.DELETED]:
            if utils.binary_search(metadata_ids, doc["uid"]) == -1:
                delete_documents.append(doc["uid"])

    if delete_documents:
        logger.info("Mark for deletion %d documents.", len(delete_documents))
        deleted += len(delete_documents)

        batches = [
            delete_documents[i : i + 1000]
            for i in range(0, len(delete_documents), 1000)
        ]
        for batch in batches:
            dto_publications.set_missing(batch, ctx.SESSION_UID)

        with open(csv_remove_output, "w", encoding="utf-8", newline="\n") as file:
            writer = csv.writer(file)
            writer.writerow(["uid"])  # header row
            for doc_uid in delete_documents:
                writer.writerow([doc_uid])

    return deleted


def _get_paths(lang_iso2: str) -> tuple[str, str, str]:
    """Get the paths for the new and remove csv files."""

    pathlib.Path(ctx.APP_CONFIG.DATA_DIR, "batches").mkdir(parents=True, exist_ok=True)

    csv_new_output = pathlib.Path(
        ctx.APP_CONFIG.DATA_DIR,
        "batches",
        f"{ctx.SESSION_UID}-cellar-new-{lang_iso2}.csv",
    )

    csv_remove_output = pathlib.Path(
        ctx.APP_CONFIG.DATA_DIR,
        "batches",
        f"{ctx.SESSION_UID}-cellar-remove-{lang_iso2}.csv",
    )

    csv_update_output = pathlib.Path(
        ctx.APP_CONFIG.DATA_DIR,
        "batches",
        f"{ctx.SESSION_UID}-cellar-update-{lang_iso2}.csv",
    )

    return csv_new_output, csv_update_output, csv_remove_output


def _write_to_csv(file_path: str, documents: list[dict]) -> None:
    """Output the documents to a csv file."""

    headers = [
        "uri",
        "do_not_index",
        "resource_legal_id_sector",
        "force",
        "date",
        "type",
        "ids",
        "subjects",
        "format",
        "mime",
        "link",
        "title",
    ]

    csv_exists = pathlib.Path(file_path).exists()
    with open(file_path, "a+", encoding="utf-8", newline="\n") as file:
        writer = csv.writer(file)

        if not csv_exists:
            writer.writerow(headers)

        for doc in documents:
            row = []
            for key in headers:
                if key not in doc:
                    row.append("")
                else:
                    row.append(doc[key])
            writer.writerow(row)
