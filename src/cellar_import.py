import logging
import pathlib
import csv

from werkzeug.utils import secure_filename

from repository import dto_publications, dto_documents
from repository.models import publication_models as models, file_status as fs

from src import context as ctx
from src.helpers import utils

logger = logging.getLogger("import")

FILE_PREFIX = "cellar-"  # prefix for the files


def import_publications(lang: str, import_dir: str) -> int:
    """Import publications into the new schema."""

    ids_file_name = f"{FILE_PREFIX}ids.csv"
    meta_file_name = f"{FILE_PREFIX}{lang}.csv"

    ids_file_path = pathlib.Path(import_dir, ids_file_name)
    if not ids_file_path.exists():
        raise FileNotFoundError(f"File not found: {ids_file_path}")

    meta_file_path = pathlib.Path(import_dir, meta_file_name)
    if not meta_file_path.exists():
        raise FileNotFoundError(f"File not found: {meta_file_name}")

    if dto_publications.any_publications():
        raise ValueError("There are already publications in the database.")

    projects = _load_ids(ids_file_path)

    if not projects:
        logger.warning("No projects to import.")
        return 0

    cnt = _import_publications(lang, meta_file_path, projects)

    if cnt > 0:
        logger.debug("Building documents ...")
        dto_documents.build_documents()

    return cnt


def _load_ids(file_path: str) -> dict:
    """Load ids from the file."""

    ids = {}

    with open(file_path, "r", encoding="utf-8") as file:
        csv_data = csv.reader(file)
        fields = next(csv_data)  # header row
        logger.debug("fields: %s", fields)

        for row in csv_data:
            row_data = dict(zip(fields, row))

            p_date = row_data.get("date", None)
            if p_date is not None:
                row_data["year"] = p_date[0:7]
            else:
                row_data["year"] = "unknown"

            uid = row_data["uri"][46:]

            ids[uid] = row_data

    return ids


def _import_publications(lang: str, file_path: str, projects_cache: dict) -> int:
    """Import lang metadata."""

    ids_cache = {}

    insert_publications: list[models.Publication] = []
    inserts = 0

    def _bulk_insert_documents():
        # Bulk insert

        nonlocal insert_publications, inserts
        if insert_publications:
            logger.debug("Inserting %d documents into the collection.", len(insert_publications))
            dto_publications.bulk_insert(insert_publications)
            inserts += len(insert_publications)

            insert_publications = []

    with open(file_path, "r", encoding="utf-8") as file:
        csv_data = csv.reader(file)
        fields = next(csv_data)  # header row
        logger.debug("fields: %s", fields)

        for row in csv_data:
            row_data = dict(zip(fields, row))
            publication = utils.build_meta_row(row_data)
            doc_uid = publication["uid"]

            if ids_cache.get(doc_uid, None) is not None:
                continue

            project = projects_cache.get(publication["project_uid"], None)

            if not project:
                continue

            ids_cache[doc_uid] = 1

            publication["uri"] = project["uri"]
            publication["date"] = project["date"]
            publication["year"] = project["year"]
            publication["type"] = project["type"]
            publication["ids"] = project["ids"]
            publication["subjects"] = project["subjects"]
            publication["do_not_index"] = project["do_not_index"]
            publication["resource_legal_id_sector"] = project["resource_legal_id_sector"]
            publication["force"] = project["force"]

            publication["lang"] = lang
            publication["file_path"] = _compute_file_location(publication)
            publication["status"] = fs.PublicationStatus.IMPORTED
            publication["created_session_uid"] = ctx.SESSION_UID
            publication["hash"] = utils.compute_publication_hash(publication)

            insert_publications.append(publication)

            if len(insert_publications) >= 50_000:
                _bulk_insert_documents()

    _bulk_insert_documents()

    return inserts


def _compute_file_location(document: dict) -> str:
    """Compute the file location."""
    project_uid = document["project_uid"]
    year = document["year"]
    lang = document["lang"]
    link = document["link"]

    ext = document["file_extension"]

    location_dir = pathlib.Path(ctx.APP_CONFIG.DATA_DIR, year, project_uid, lang).as_posix()
    filename = secure_filename(link + "." + ext) + ".xml"

    return pathlib.Path(location_dir, filename).as_posix()
