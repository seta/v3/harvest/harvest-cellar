REQUEST_HEADERS = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:126.0) Gecko/20100101 Firefox/126.0"
}

SOURCE_NAME = "cellar"
SOURCE = "op-cellar"

SECTOR_CODES = {
    "1": "Treaty",
    "2": "International agreements",
    "3": "Secondary legislation",
    "4": "Complementary legislation",
    "5": "Preparatory act",
    "6": "EU case-law",
    "7": "National implementing measures",
    "8": "National case-law",
    "9": "Parliamentary questions",
    "0": "Consolidated acts",
    "C": "OJ C series",
    "E": "EFTA documents",
}

URI_ROOT = "http://publications.europa.eu/resource/cellar/"
SPARQL_ENDPOINT = "https://publications.europa.eu/webapi/rdf/sparql"
