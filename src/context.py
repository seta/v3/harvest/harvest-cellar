import sqlalchemy as db
from config import config

APP_CONFIG: config.Config = None
SESSION_UID: str = None

MAIN_ENGINE: db.engine.Engine = None
PUBLICATIONS_ENGINE: db.engine.Engine = None

INGEST_ENGINE: db.engine.Engine = None

RESUME_LAST: bool = False
TEST = False


def init_context(
    app_config: config.Config,
    session_uid: str,
    main_engine: db.engine.Engine,
    publications_engine: db.engine.Engine = None,
    ingest_engine: db.engine.Engine = None,
    resume_last: bool = False,
    test: bool = False,
):
    """Initialize the context."""

    # pylint: disable=global-statement
    global APP_CONFIG, SESSION_UID, MAIN_ENGINE, PUBLICATIONS_ENGINE, INGEST_ENGINE, TEST, RESUME_LAST

    APP_CONFIG = app_config
    SESSION_UID = session_uid
    MAIN_ENGINE = main_engine
    PUBLICATIONS_ENGINE = publications_engine
    INGEST_ENGINE = ingest_engine
    TEST = test
    RESUME_LAST = resume_last
