import logging
import pathlib
import time
import requests

from werkzeug.utils import secure_filename

from src import context as ctx, constants
from src.helpers import nlp_client

from repository import dto_publications
from repository.models import publication_models, file_status as fs


logger = logging.getLogger("download")
request_session = requests.Session()


INCREMENTAL_LIMIT = 1000
SLEEP_TIME = 30


def incremental_work(lang: str) -> int:
    """Download and parse new documents."""
    downloads = 0

    count_new_docs = dto_publications.count_for_download()
    logger.info("Downloading %d new documents for %s.", count_new_docs, lang)

    index = 0

    while index <= count_new_docs:
        limit = INCREMENTAL_LIMIT
        new_docs = dto_publications.get_all_for_download(limit=limit)

        if new_docs is not None and len(new_docs) > 0:
            logger.info("Downloading %d documents from index %s.", len(new_docs), index)
            for document in new_docs:
                downloaded = download_publication(document)
                if downloaded:
                    downloads += 1
        else:
            logger.info("No new documents to download.")
            break

        if ctx.TEST:
            break

        index += INCREMENTAL_LIMIT

        if index <= count_new_docs and downloads > 0:
            logger.info("Sleeping for %s seconds on index % s...", SLEEP_TIME, index)
            time.sleep(SLEEP_TIME)

    return downloads


def delete_publications():
    """Remove downloaded file and set obsolete documents as deleted."""

    publications = dto_publications.get_missing()

    if publications:
        logger.info("Deleting %d publications.", len(publications))

        ids = []

        for publication in publications:
            ids.append(publication.uid)

            if publication.file_path:
                pathlib.Path(publication.file_path).unlink(missing_ok=True)

        if ids:
            dto_publications.bulk_update_status(ids, fs.PublicationStatus.DELETED)


def download_publication(publication: publication_models.Publication) -> bool:
    """Download a publication."""

    if publication.file_extension in ctx.APP_CONFIG.EXTENSIONS_SKIP:
        logger.info("Skipping %s, invalid extension defined in app.conf.", publication.link)

        dto_publications.update_status(publication.uid, fs.PublicationStatus.INVALID)
        return False

    location_dir = _get_document_location(publication)
    filename = secure_filename(publication.link + "." + publication.file_extension) + ".xml"

    file_location = pathlib.Path(location_dir, filename).as_posix()
    content_size = None
    file_size = None

    # check if file already exists
    if publication.status == fs.PublicationStatus.NEW and pathlib.Path(file_location).exists():
        file_size = pathlib.Path(file_location).stat().st_size

        dto_publications.set_downloaded(
            uid=publication.uid,
            file_path=file_location,
            content_size=content_size,
            file_size=file_size,
        )
        return False

    try:
        # download the file
        download_response = request_session.get(publication.link, headers=constants.REQUEST_HEADERS, timeout=300)
        download_response.raise_for_status()

        content_length = download_response.headers.get("content-length")
        if content_length is None:
            content_size = len(download_response.content)

        outs = nlp_client.parse_file(download_response.content)

        pathlib.Path(location_dir).mkdir(parents=True, exist_ok=True)
        with open(file_location, "wb") as f:
            f.write(outs)

        file_size = pathlib.Path(file_location).stat().st_size

    except requests.exceptions.RequestException as e:
        status_code = 404
        if e.response:
            status_code = e.response.status_code

        logger.error("Error downloading %s: %s", publication.link, e)
        logger.exception(e)

        dto_publications.update_status(publication.uid, f"{fs.PublicationStatus.ERROR_ON_DOWNLOAD}-{status_code}")

        return False

    except Exception as e:
        logger.error("Error parsing document %s: %s", publication.link, e)
        logger.exception(e)

        dto_publications.update_status(publication.link, fs.PublicationStatus.ERROR_ON_PARSE)

        return False

    logger.debug(
        "File downloaded %s to %s, content size %s, file size %s",
        publication.link,
        file_location,
        content_size,
        file_size,
    )
    dto_publications.set_downloaded(
        uid=publication.uid,
        file_path=file_location,
        content_size=content_size,
        file_size=file_size,
    )

    return True


def _get_document_location(publication: publication_models.Publication) -> str:
    """Get the document location."""
    return pathlib.Path(
        ctx.APP_CONFIG.DATA_DIR,
        publication.year,
        publication.project_uid,
        publication.lang,
    ).as_posix()
