from diskcache import Cache

from .helpers import resource_types
from . import context as ctx

# this can be left open
_cache: Cache = None


def init_cache() -> None:
    """Initialize the resource types cache."""

    global _cache  # pylint: disable=global-statement

    _cache = Cache(directory=ctx.APP_CONFIG.SQLITE_DIR, tag_index=True)

    _cache.expire()

    list_length = _cache.get("resource_types_length")
    expire_seconds = 60 * 60 * 24 * 7

    if list_length is None:
        _cache.evict(tag="resource_types")
        rt_list = resource_types.get_resource_types()

        for rt in rt_list:
            _cache.add(
                rt[0],
                rt[1],
                expire=expire_seconds,
                tag="resource_types",
            )

        _cache.add(
            "resource_types_length",
            len(rt_list),
            expire=expire_seconds,
            tag="resource_types",
        )


def key_in_cache(key: str) -> bool:
    """Check if a key is in the cache."""
    return _cache.get(key) is not None


def get_from_cache(key: str):
    """Get a value from the cache."""
    return _cache.get(key)


def close_cache() -> None:
    """Close the cache."""
    if _cache is not None:
        _cache.close()
