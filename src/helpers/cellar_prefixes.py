""""
Gets the id prefixes from the cellar as the first two characters of the document id (the last part of uri).
"""

import csv
from SPARQLWrapper import SPARQLWrapper, CSV

from src.constants import SPARQL_ENDPOINT

QUERY = """
prefix cdm: <http://publications.europa.eu/ontology/cdm#>

select ?prefix
WHERE {{
       ?uri cdm:work_id_document ?doc_id.
        BIND (SUBSTR(STR(?uri), 47, {length}) as ?prefix).
    }}
GROUP BY ?prefix
"""


def get_prefixes(length: int = 1) -> list[str]:
    """Gets the id prefixes from the cellar as the first two characters of the document id (the last part of uri)."""
    sparql = SPARQLWrapper(SPARQL_ENDPOINT, returnFormat=CSV)
    sparql.setTimeout(1200)

    sparql.setQuery(QUERY.format(length=length))
    result = sparql.queryAndConvert()

    lines = result.decode("utf-8").splitlines()
    prefixes = []

    csv_data = csv.reader(lines)

    next(csv_data, None)  # header row

    for row in csv_data:
        prefixes.append(row[0])

    prefixes.sort()

    return prefixes
