QUERY = """
prefix cdm: <http://publications.europa.eu/ontology/cdm#>
prefix cmr: <http://publications.europa.eu/ontology/cdm/cmr#>

SELECT
    ?uri 
    ?do_not_index 
    ?resource_legal_id_sector 
    ?force 
    ?date 
    ?type
    ?ids
    ?subjects
    ?format 
    ?mime
    ?link_exp
    ?title
WHERE
{{
	{{
		select distinct
			?uri 
			?do_not_index 
			?resource_legal_id_sector 
			?force 
			?date 
			(group_concat(distinct STR(?resource_type);separator=",") as ?type) 
			(group_concat(distinct STR(?doc_id);separator=",") as ?ids) 
			(group_concat(distinct STR(?subject_);separator=",") as ?subjects)
			?format 
			?mime
			?link_exp
			?title
		where 
		{{

			{{
				select ?uri 
				WHERE 
				{{ 
				   ?uri cdm:work_id_document ?v.
				   FILTER(SUBSTR(STR(?uri), 47, {prefix_length}) = "{prefix}")
				}}
				GROUP BY ?uri 
			}}
 
			?uri cdm:work_id_document ?doc_id.
			
			OPTIONAL{{?uri cdm:work_has_resource-type ?resource_type.}}
			OPTIONAL{{?uri cdm:work_date_document ?date.}}
			OPTIONAL{{?uri cdm:resource_legal_in-force ?force.}}
			OPTIONAL{{?uri cdm:do_not_index ?do_not_index.}}
			OPTIONAL{{?uri cdm:resource_legal_id_sector ?resource_legal_id_sector.}}
			OPTIONAL {{?uri cdm:work_is_about_concept_eurovoc ?subject_.}}
			?exp cdm:expression_belongs_to_work ?uri .
			?exp cdm:expression_title ?title.
			?exp cdm:expression_uses_language <http://publications.europa.eu/resource/authority/language/{lang}>.
			?uri_manif cdm:manifestation_manifests_expression ?exp;
					   cdm:manifestation_type ?format.
			?link_exp cdm:item_belongs_to_manifestation ?uri_manif.
			?link_exp cmr:manifestationMimeType ?mime.			
		}} 
	}}
	FILTER(!(?mime in ("application/xml;type=fmx4","image/jpeg","image/tiff","application/zip")))
    FILTER(lang(?title) in ("{lang_iso2}", "")).
}}
"""


def get_query(lang: str, lang_iso2: str, prefix: str) -> str:
    """Gets cellar query."""

    return QUERY.format(
        lang=lang, lang_iso2=lang_iso2, prefix=prefix, prefix_length=len(prefix)
    )
