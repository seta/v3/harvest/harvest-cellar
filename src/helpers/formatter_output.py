import json
import hashlib
import pathlib
import logging

from src.helpers import tika_helpers, nlp_client

logger = logging.getLogger("formatter")


IGNORE_TEXT_LENGTH = 500


def _add_crc(js_doc: dict):
    """Add the CRC to the json document."""

    json_str = json.dumps(js_doc, sort_keys=True)
    sha = hashlib.sha256()
    sha.update(json_str.encode("ascii"))

    if "other" not in js_doc:
        js_doc["other"] = {}

    js_doc["other"]["crc"] = sha.hexdigest()


def write_output(
    json_doc: dict,
    input_file: str,
    input_file_ext: str,
    output_file: str,
    one_chunk_per_doc: bool = False,
) -> tuple[bool, int, int, int]:
    """
    Output json chunks.

    :param json_doc: the json document
    :param input_file: the input file path
    :param input_file_ext: the input file extension
    :param output_file: the output file path

    Returns:
        bool: True if the document was processed
        int: the length of the text inside input file
        int: size of the transformed text
        int: size of the complete text (title + transformed text)
    """

    if not pathlib.Path(input_file).exists():
        return False, 0, 0, 0

    with open(input_file, "r", encoding="utf-8") as file:
        text = file.read()

    t_len = len(text)
    if t_len <= 1000:
        return False, t_len, 0, 0

    if input_file_ext.lower() in ["html", "xhtml"]:
        transformed_text = tika_helpers.html2text(text)
    else:
        transformed_text = tika_helpers.tika2text(text)

    json_doc["text"] = transformed_text
    _add_crc(json_doc)
    del json_doc["text"]
    tt_len = len(transformed_text)

    complete_text = json_doc["title"] + "\n" + transformed_text + "\n"
    ct_len = len(complete_text)
    if ct_len <= IGNORE_TEXT_LENGTH:
        return False, t_len, tt_len, ct_len

    if one_chunk_per_doc:
        json_doc["chunk_text"] = complete_text
        json_doc["chunk_number"] = 1
        json_doc["sbert_embedding"] = None

        try:
            with open(output_file, "a", encoding="utf-8") as file:
                file.write(json.dumps(json_doc) + "\n")
        except Exception as e:
            logger.exception("Error writing output file: %s", e)
            return False, t_len, tt_len, ct_len
    else:
        try:

            embeddings = nlp_client.compute_embeddings(complete_text)

            with open(output_file, "a", encoding="utf-8") as file:
                for emb in embeddings:
                    json_doc["chunk_text"] = emb["text"]
                    json_doc["chunk_number"] = emb["chunk"]
                    json_doc["sbert_embedding"] = emb.get("vector", None)

                    file.write(json.dumps(json_doc) + "\n")
        except Exception as e:
            logger.exception("Error writing output file: %s", e)
            return False, t_len, tt_len, ct_len

    return True, t_len, tt_len, ct_len
