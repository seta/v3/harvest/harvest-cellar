from urllib.parse import urljoin
import requests

from src import context as ctx


def parse_file(file_content: bytes) -> bytes:
    """Parse a file using the NLP service."""

    with requests.Session() as nlp_session:
        response = nlp_session.post(
            urljoin(ctx.APP_CONFIG.NLP_API_URL, "file_to_text"),
            files={"file": file_content},
            headers={"Accept": "text/xml"},
        )

        response.raise_for_status()
        return response.content


def compute_embeddings(text: str) -> list[dict]:
    """Compute embeddings for a given text."""
    with requests.Session() as nlp_session:
        response = nlp_session.post(
            urljoin(ctx.APP_CONFIG.NLP_API_URL, "compute_embeddings"),
            json={"text": text},
        )

        response.raise_for_status()

        json = response.json()
        return json["emb_with_chunk_text"]
