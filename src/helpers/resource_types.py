import requests

from src import formatter_cache as fc
from src import constants

PUBLICATION_URL = "http://publications.europa.eu/resource/authority/resource-type"


def get_resource_types():
    """Get the resource types from the EU publications office."""

    resource_types = []
    response = requests.get(
        PUBLICATION_URL, headers=constants.REQUEST_HEADERS, timeout=300
    )

    if response.status_code == 200:

        # TODO: parse text using lxml

        rs = response.text.split(
            '<rdf:Description rdf:about="http://publications.europa.eu/resource/authority/resource-type/'
        )

        for r in rs[1:]:
            r = r.split('"')[0]

            nw = requests.get(
                PUBLICATION_URL + "/" + r,
                headers=constants.REQUEST_HEADERS,
                timeout=300,
            )

            if nw.status_code == 200:
                n = nw.text.split('<skos:prefLabel xml:lang="en">')[1].split(
                    "</skos:prefLabel>"
                )[0]
                resource_types.append((r, n))

    return resource_types


def get_reference(resource_type: str):
    """Get the reference from the resource types."""

    types = resource_type.split(",")
    if len(types) == 1:
        dtt = resource_type.split("/")[-1]

        cache_dtt = fc.get_from_cache(dtt)

        if cache_dtt is not None:
            return cache_dtt + ":" + dtt
        return None

    ls = []

    if (
        "http://publications.europa.eu/resource/authority/resource-type/CORRIGENDUM"
        in types
    ):
        cache_value = fc.get_from_cache("CORRIGENDUM")
        if cache_value is None:
            cache_value = "CORRIGENDUM"
        ls.append(cache_value + ":CORRIGENDUM")
    elif (
        "http://publications.europa.eu/resource/authority/resource-type/PUB_GEN"
        in types
    ):
        cache_value = fc.get_from_cache("PUB_GEN")
        if cache_value is None:
            cache_value = "PUB_GEN"
        ls.append(cache_value + ":PUB_GEN")

    for rt in types:
        lst = rt.split("/")[-1]
        if lst not in ("CORRIGENDUM", "PUB_GEN"):
            cache_value = fc.get_from_cache(lst)
            if cache_value is not None:
                ls.append(cache_value + ":" + lst)

    return ",".join(ls)
