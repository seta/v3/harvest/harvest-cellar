import time


class Stopwatch:
    digits: int

    def __init__(self, digits: int = 2):
        self.digits = digits

        self._start = time.perf_counter()
        self._end = None

    @property
    def duration(self) -> float:
        """Get the duration of the stopwatch."""

        return (
            self._end - self._start if self._end else time.perf_counter() - self._start
        )

    @property
    def running(self) -> bool:
        """Check if the stopwatch is running."""
        return not self._end

    def restart(self) -> None:
        """Restart the stopwatch."""
        self._start = time.perf_counter()
        self._end = None

    def reset(self) -> None:
        """Reset the stopwatch."""
        self._start = time.perf_counter()
        self._end = self._start

    def start(self) -> None:
        """Start the stopwatch."""
        if not self.running:
            self._start = time.perf_counter() - self.duration
            self._end = None

    def stop(self) -> None:
        """Stop the stopwatch."""
        if self.running:
            self._end = time.perf_counter()

    def __str__(self) -> str:
        duration = self.duration

        if duration >= 1:
            return f"{duration:.{self.digits}f}s"

        if duration >= 0.01:
            return f"{duration * 1000:.{self.digits}f}ms"

        return f"{duration * 1000 * 1000:.{self.digits}f}μs"
