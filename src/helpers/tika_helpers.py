import string
import re
from itertools import chain
from lxml import etree as ET


def _clean_repetitions(tt):

    tt = re.sub("\+ \+( \+)+", "...", tt)
    tt = re.sub("\. \.( \.)+", "...", tt)
    tt = re.sub("\.\.(\.)+", "...", tt)
    tt = re.sub("��(�)+", "���", tt)
    tt = re.sub(">>(>)+", ">>>", tt)
    tt = re.sub("<<(<)+", "<<<", tt)
    tt = re.sub("> > (> )+", "> > > ", tt)
    tt = re.sub(" < <( <)+", " < < <", tt)
    tt = re.sub("__(_)+", "___", tt)
    tt = re.sub("--(-)+", "---", tt)
    tt = re.sub("~~(~)+", "~~~", tt)
    tt = re.sub("\+\+(\+)+", "+++", tt)
    tt = re.sub("##(#)+", "###", tt)
    tt = re.sub("%%(%)+", "%%%", tt)
    tt = re.sub("\*\*(\*)+", "***", tt)
    tt = re.sub(" ( )+", " ", tt)

    return tt


def _cleantext(txt):

    tr = []
    for tt in txt:
        tt = tt.strip()
        tt = " ".join(tt.split("\t"))
        tt = " ".join(tt.split(" "))
        tr.append(tt)
    return tr


def _lines_to_line(lns):
    return " ".join(lns.split("\n"))


def _joinp(ps):
    d = string.digits
    ds = list(map(lambda d1: list(map(lambda d2: d1 + d2, tuple(d))), tuple(d)))
    ds = list(chain(*ds))
    sp = ds + list(string.ascii_letters)
    sp1 = list(map(lambda c: c + ")", sp))
    sp2 = list(map(lambda c: c + ".", sp))
    sp3 = list(map(lambda c: "(" + c + ")", sp))

    ss = sp1 + sp2 + sp3 + ["-", "–", "•", "▼", "—"]

    if len(ps) > 1:
        ps2 = []
        p1 = ps[0]
        for p2 in ps[1:]:
            if len(p2) > 0 and len(p1) > 0:
                if (
                    len(p1.split("\n")) == 1
                    and len(p2.split("\n")) == 1
                    and len(p2) < 200
                    and (not p2.startswith(tuple(ss)))
                    and p1[-1] not in ".:;!?\"'"
                ):
                    p1 = p1 + " " + p2
                else:
                    p1 = _clean_repetitions(_lines_to_line(p1))
                    if (
                        len(p1) > 8
                    ):  # sometimes pdf text extraction fails and we have many lines with few symbols only.
                        ps2.append(p1)
                    p1 = p2
            elif len(p1) > 0:
                p1 = _clean_repetitions(_lines_to_line(p1))
                if len(p1) > 8:
                    ps2.append(p1)
                p1 = p2
            else:
                p1 = p2

        p1 = _clean_repetitions(_lines_to_line(p1))

        if len(p1) > 8:
            ps2.append(p1)

        return ps2

    return ps


def tika2text(txt):
    """Converts tika output to text."""

    text: str = ""
    txt: str = (txt.split("</html>", 1)[0]) + "</html>"

    if len(txt) > 1000:
        nsmap = {"a": "http://www.w3.org/1999/xhtml"}

        pa = ET.XMLParser(huge_tree=True)
        root = ET.fromstring(text=txt.encode(), parser=pa)

        ts = []
        r = root.xpath(".//a:p", namespaces=nsmap)

        for p in r:
            ts.append("\n".join(_cleantext([t.strip() for t in p.itertext()])))
        ts = _joinp(ts)

        txt = "\n".join(ts)
        text = txt + "\n\n"

    return text


def html2text(txt):
    """Converts html to text."""

    text = ""
    txt = (txt.split("</html>", 1)[0]) + "</html>"

    if len(txt) > 1000:
        nsmap = {"a": "http://www.w3.org/1999/xhtml"}
        pa = ET.XMLParser(huge_tree=True)
        root = ET.fromstring(txt.encode(), parser=pa)
        ts = []

        body = root.xpath(".//a:body", namespaces=nsmap)
        r = body[0].xpath(
            ".//a:p | .//a:title | .//a:h1 | .//a:h2 | .//a:li", namespaces=nsmap
        )

        for p in r:
            ts.append("\n".join(_cleantext([t.strip() for t in p.itertext()])))
        ts = _joinp(ts)
        txt = "\n".join(ts)
        text = txt + "\n\n"

    return text
