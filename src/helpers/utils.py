import hashlib


def binary_search(arr, x) -> int:
    """Binary search in ordered array."""

    l = 0
    r = len(arr) - 1
    while l <= r:
        mid = l + (r - l) // 2
        if arr[mid] == x:
            return mid

        if arr[mid] < x:
            l = mid + 1
        else:
            r = mid - 1

    return -1


def binary_search_dicts(entries: list[dict], field: str, value: str) -> dict | None:
    """Binary search in ordered dict."""

    l = 0
    r = len(entries) - 1
    while l <= r:
        mid = l + (r - l) // 2
        if entries[mid][field] == value:
            return entries[mid]

        if entries[mid][field] < value:
            l = mid + 1
        else:
            r = mid - 1

    return None


def build_meta_row(row_data: dict, include_project_info: bool = False) -> dict:
    """Builds a meta row from a list of strings."""

    project_uid = row_data["uri"][46:]

    link_exp = row_data["link_exp"]

    ### ids
    # http://publications.europa.eu/resource/cellar/0c981432-b137-4614-a2fc-1738252cd856.0004.01/DOC_1
    # uid = 0c981432-b137-4614-a2fc-1738252cd856.0004.01/DOC_1
    # doc_id = 0c981432-b137-4614-a2fc-1738252cd856.0004/DOC_1
    uid = link_exp[46:]

    lns = uid.split("/")  # two parts
    lns[0] = ".".join(lns[0].split(".")[:-1])
    doc_id = "/".join(lns)
    #### end ids

    ext = row_data["mime"].split("/")[-1]
    ext = ext.split(";")[0].split("+")[0]
    ext = ext.split("+")[0]

    row_data["file_extension"] = ext

    format_priority = 0
    if ext == "pdf":
        format_priority = 10
    elif ext == "xhtml":
        format_priority = 9
    elif ext == "html":
        format_priority = 8

    meta = {
        "uid": uid,
        "link": link_exp,
        "project_uid": project_uid,
        "format": row_data["format"],
        "mime": row_data["mime"],
        "title": row_data["title"],
        "file_extension": ext,
        "doc_id": doc_id,
        "format_priority": format_priority,
    }

    if include_project_info:
        meta["uri"] = row_data["uri"]
        meta["date"] = row_data["date"]
        meta["year"] = row_data.get("date", "unknown")[0:7]
        meta["type"] = row_data["type"]
        meta["ids"] = row_data["ids"]
        meta["subjects"] = row_data["subjects"]
        meta["do_not_index"] = row_data["do_not_index"]
        meta["resource_legal_id_sector"] = row_data["resource_legal_id_sector"]
        meta["force"] = row_data["force"]

    return meta


def compute_publication_hash(publication: dict) -> str:
    """Compute the publication hash."""

    return _compute_dict_hash(
        {
            "uri": publication["uri"],
            "date": publication["date"],
            "type": publication["type"],
            "ids": publication["ids"],
            "subjects": publication["subjects"],
            "do_not_index": publication["do_not_index"],
            "resource_legal_id_sector": publication["resource_legal_id_sector"],
            "force": publication["force"],
            "format": publication["format"],
            "mime": publication["mime"],
            "title": publication["title"],
        }
    )


def _compute_dict_hash(data: dict) -> str:
    """Compute the hash of a dictionary."""
    return hashlib.md5(str(sorted(data.items())).encode()).hexdigest()
