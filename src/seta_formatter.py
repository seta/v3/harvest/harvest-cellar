import logging

import pathlib

from src import context as ctx
from src.helpers import resource_types, formatter_output
from src.constants import SECTOR_CODES, SOURCE, SOURCE_NAME

from repository import dto, utils
from repository import dto_ingest, dto_documents
from repository.models import publication_models

logger = logging.getLogger("formatter")

BATCH_SIZE = 10_000
TEST_BATCH_SIZE = 1000

OUTPUT_FILE = "cellar_{0}_{1}_{2}_{3}.json"


def build_documents(lang: str):
    """Build the documents for formatting."""

    logger.info("Starting build documents ...")

    publications_engine = dto.init_publications_db(
        utils.publications_db(ctx.APP_CONFIG.SQLITE_DIR, lang=lang, test_mode=ctx.TEST),
        echo=False,
    )

    try:
        dto_documents.build_documents(engine=publications_engine)
    finally:
        publications_engine.dispose()


def start_formatter(
    lang: str, lang_iso2: str, complete_text: bool = False
) -> tuple[int, int]:
    """Start the formatter for language documents."""

    logger.info("Starting formatter for %s ...", lang)

    publications_engine = dto.init_publications_db(
        utils.publications_db(ctx.APP_CONFIG.SQLITE_DIR, lang=lang, test_mode=ctx.TEST),
        echo=False,
    )

    try:
        docs_count = dto_documents.get_format_count(publications_engine)

        if docs_count == 0:
            logger.info("No documents to format.")

            return 0, 0

        logger.info("Total documents to format: %d", docs_count)

        index = 1
        total_processed = 0
        output_files = 0

        output_batch_size = BATCH_SIZE if not ctx.TEST else TEST_BATCH_SIZE

        # if ctx.TEST and docs_count > 30:
        #    docs_count = 30

        while index <= docs_count:

            output_files += 1

            update_documents = []
            error_documents = []
            publication_uids = []

            output_file_name = OUTPUT_FILE.format(
                ctx.SESSION_UID, lang, index, index + output_batch_size
            )
            output_file_path = pathlib.Path(
                ctx.APP_CONFIG.OUTPUT_DIR,
                output_file_name,
            ).as_posix()

            documents = dto_documents.get_format_documents(
                publications_engine, limit=output_batch_size
            )

            processed_no = 0

            for document in documents:
                publication = document.publication
                publication_uids.append(publication.uid)

                if not publication.date:
                    logger.warning(
                        "Project date not found for uid %s, load aborted",
                        publication.project_uid,
                    )

                    error_documents.append(publication.doc_id)

                    continue

                json_doc = _format_document(publication, lang_iso2)

                processed, t_len, tt_len, ct_len = formatter_output.write_output(
                    json_doc=json_doc,
                    input_file=publication.file_path,
                    input_file_ext=publication.file_extension,
                    output_file=output_file_path,
                    one_chunk_per_doc=complete_text,
                )

                if processed:
                    processed_no += 1
                    update_documents.append(publication.doc_id)
                else:
                    error_documents.append(publication.doc_id)

                logger.info(
                    "Doc_id: %s, output_file: %s, Processed: %s, doc_len: %s, text_len: %s, complete_len: %s",
                    publication.doc_id,
                    output_file_name,
                    processed,
                    t_len,
                    tt_len,
                    ct_len,
                )

            if update_documents or error_documents:
                logger.debug(
                    "Updating %d processed documents.",
                    len(update_documents) + len(error_documents),
                )
                dto_documents.batch_processed(
                    engine=publications_engine,
                    update_doc_ids=update_documents,
                    error_doc_ids=error_documents,
                    publication_uids=publication_uids,
                    output_file_name=output_file_name,
                )

            if processed_no > 0:
                dto_ingest.insert(
                    file_path=output_file_path, entries=processed_no, lang=lang
                )

                total_processed += processed_no

            index += output_batch_size

        return total_processed, output_files
    finally:
        publications_engine.dispose()


def ready_for_deletion(lang: str) -> int:
    """Ready deleted documents"""

    logger.info("Starting ready deletion for %s ...", lang)

    publications_engine = dto.init_publications_db(
        utils.publications_db(ctx.APP_CONFIG.SQLITE_DIR, lang=lang, test_mode=ctx.TEST),
        echo=False,
    )

    try:
        deleted_docs = dto_documents.get_deleted_documents(publications_engine)

        update_ids = []
        insert_ids = []

        i = 0
        total_deleted = len(deleted_docs)

        while i < total_deleted:
            doc = deleted_docs[i]

            insert_ids.append(_document_id(doc.publication.link))
            update_ids.append(doc.doc_id)

            if len(insert_ids) >= BATCH_SIZE or i == total_deleted - 1:
                dto_ingest.batch_insert_deleted(insert_ids)
                dto_documents.batch_set_ready_for_deletion(
                    update_ids, publications_engine
                )

                insert_ids = []
                update_ids = []

                logger.debug("Deleted %s documents out of %s", i + 1, total_deleted)

            i += 1

        return total_deleted
    finally:
        publications_engine.dispose()


def _document_id(link: str) -> str:
    """Get the document id from the link."""

    if link[0] == '"':
        link = link[1:-1]  # remove quotes

    return SOURCE_NAME + ":" + link


def _format_document(
    publication: publication_models.Publication, lang_iso2: str
) -> dict:
    """Format the document."""

    json_doc = {
        "id": SOURCE_NAME + ":" + publication.project_uid,
        "source": SOURCE,
        "mime_type": publication.mime,
        "language": lang_iso2,
        "collection": "unknown",
        "reference": "unknown",
    }

    if publication.ids:
        json_doc["id_alias"] = publication.ids

    link = publication.link
    if link[0] == '"':
        link = link[1:-1]  # remove quotes
    json_doc["link_origin"] = link

    json_doc["document_id"] = _document_id(link)

    json_doc["date"] = publication.date.split(" ")[0]
    json_doc["title"] = publication.title.replace("\n", " ")

    if publication.force == "0":
        json_doc["in_force"] = "false"
    elif publication.force == "1":
        json_doc["in_force"] = "true"
    else:
        json_doc["in_force"] = "unk"

    if publication.resource_legal_id_sector in SECTOR_CODES:
        collection = SECTOR_CODES.get(publication.resource_legal_id_sector, None)
        if collection is not None:
            json_doc["collection"] = collection

    if publication.type is not None:
        reference = resource_types.get_reference(publication.type)
        if reference is not None:
            json_doc["reference"] = reference

    if publication.subjects is not None and publication.subjects != "":
        json_doc["taxonomy"] = []
        subjects = publication.subjects.split(",")
        for subject in subjects:
            subject = subject.strip()  # Trim leading and trailing whitespace
            if subject != "":
                json_doc["taxonomy"].append("eurovoc:" + subject.split("/")[-1])

    return json_doc
